﻿$(document).ready(function () {

	$(".brand").click(function () {
		
		var menu = $("#menuOpcoes");

		if (menu.hasClass("hidden")) {
			menu.slideDown(200).removeClass("hidden");
		} else {
			menu.slideUp(200, function() {
				menu.addClass("hidden");
			});
		}
	});

});